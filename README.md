# SADX Dreamcast DLC mod by PkR

This is a mod that recreates Dreamcast Sonic Adventure events and challenges in the PC version of Sonic Adventure DX.

To install Dreamcast DLC you can use SADX Mod Installer that sets up SADX Mod Loader and downloads the latest versions of mods automatically. The installer can also convert the Steam version of SADX to the 2004 version, which supports the Mod Loader. 

**SADX Mod Installer:** https://sadxmodinstaller.unreliable.network

Alternatively you could install Dreamcast DLC manually by downloading the latest version [here](https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/DLCs.7z).

**Related mods:**

Dreamcast Conversion: https://github.com/PiKeyAr/sadx_dreamcast

Sound Overhaul: https://github.com/PiKeyAr/sadx-sound-overhaul

HD GUI 2: https://github.com/PiKeyAr/sadx-hd-gui

Time of Day: https://github.com/PiKeyAr/sadx-timeofday-mod
